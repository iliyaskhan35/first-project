import { Component} from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, FormArray } from "@angular/forms";

@Component({
    selector:'app-form-example',
    templateUrl:'./form-example.html'
})
export class FormExample{
    title = 'helloworld';
    public todeoForm: FormGroup;
    public submitted = false;
    public passwordcontrol: FormControl = new FormControl('password', [Validators.required, Validators.minLength(5)]);
    constructor() {
      this.todeoForm = new FormGroup({
        name: new FormControl('', Validators.required),
        password: this.passwordcontrol,
        likes: new FormArray([
          new FormControl('')
        ]),
        addressarray: new FormArray([
          this.morecontrol
        ]),
        address: new FormGroup({
          line1: new FormControl('line1', Validators.required),
          line2: new FormControl('line2'),
          line3: new FormGroup({
            pincode: new FormControl('pincode'),
            phone: new FormGroup({
              mobile: new FormControl("Phone")
            })
          })
          })
      })
    }
  
    get getmorecontrols() {
      return this.todeoForm.get('likes') as FormArray;
    }
    addikescontrol() {
      this.getmorecontrols.push(new FormControl(''));
    }
  
    get getaddressarray() {
      return this.todeoForm.get('addressarray') as FormArray;
    }
    get morecontrol() {
      return new FormGroup({
        line1: new FormControl('line1', Validators.required),
        line2: new FormControl('line2')
      })
    }
    addmoreAddress() {
      this.getaddressarray.push(this.morecontrol);
    }
    mysubmit() {
      this.submitted = true;
      console.log(this.todeoForm);
      // console.log(this.todeoForm.controls.address.controls.line3.controls.pincode)
    }
  
    get name(): AbstractControl {
      return this.todeoForm.get('name');
    }
    get nameError() {
      return (this.name.errors && (this.name.dirty || this.name.touched)) || (this.todeoForm.invalid && this.submitted)
    }
    get line1() {
      return this.todeoForm.get('address.line1')
    }
    get pincode() {
      // return this.todeoForm.controls.address.controls.line3.controls.pincode
      return this.todeoForm.get('address.line3.pincode')
    }
    get mobile() {
      return this.todeoForm.get('address.line3.pincode.phone.mobile')
    }
    /*
    getControl(name,validation){
      return new FormControl(name, validation);
    }
    */
}