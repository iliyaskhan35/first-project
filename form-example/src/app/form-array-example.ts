import { Component } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators, AbstractControl, ValidationErrors } from "@angular/forms";
@Component({
    selector: 'app-form-array-example',
    templateUrl: './form-array-example.html'
})
export class AppFormArrayExample {
    public loginForm: FormGroup;
    constructor(public fb: FormBuilder) {
        this.loginForm = this.fb.group({
            name: new FormControl('', [Validators.required, this.myValidation]),
            phone: new FormArray([
                new FormControl(''),
                new FormControl('')
            ]),
            address: this.fb.array([
                this.addressModel
            ])
        }, this.myformlevelvalidation)
    }
    myformlevelvalidation(form: FormGroup): ValidationErrors {
        if (form.controls.name.value.length > 4) {
            return { formlevel: true }
        } else {
            return null
        }
    }
    myValidation(control: AbstractControl): ValidationErrors {
        if (control.value.length > 4) {
            return { myerror: true }

        } else {
            return null
        }
    }
    get addressModel() {
        return new FormGroup({
            line1: new FormControl(''),
            line2: new FormControl(''),
            pincode: new FormControl(''),
        })
    }
    get addressControlArray() {
        return this.loginForm.get('address') as FormArray;
    }
    addAddress() {
        this.addressControlArray.push(this.addressModel);
    }
    get phoneControlArray() {
        return this.loginForm.get('phone') as FormArray;
    }
    addMorePhone() {
        this.phoneControlArray.push(new FormControl(''));
        this.phoneControlArray.push(new FormControl(''))

    }
    submit() {
        console.log(this.loginForm)
    }
}