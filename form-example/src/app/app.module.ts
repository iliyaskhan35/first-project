import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppComponent } from './app.component';
import { FormExample } from "./form-example";
import { AppFormArrayExample } from "./form-array-example";

import { Routes, RouterModule } from '@angular/router';
const myroute: Routes = [
  //{ path: '', component: homeComponent },
  { path: 'completeform', component: FormExample },
  { path: 'form/array/example', component: AppFormArrayExample },
  // { path: '**', component: component404 },
]
@NgModule({
  declarations: [
    AppComponent,
    FormExample,
    AppFormArrayExample
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(myroute)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
